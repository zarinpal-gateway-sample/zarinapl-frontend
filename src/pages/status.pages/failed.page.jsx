import {
    Box,
    Card,
    CardContent,
    Button,
    Typography,
} from "@mui/material";

import { useHistory } from "react-router-dom";

import FailedPicture from "../../assets/failed.png";

const FailedPage = () => {
    const history = useHistory();

    return (
        <Box>
            <Card
                variant="elevation"
                elevation={18}
            >
                <CardContent sx={{ textAlign: "center" }}>
                    <Box
                        component="img"
                        alt="Failed picture"
                        src={FailedPicture}
                    />
                    <Typography
                        variant="h4"
                        color="error"
                        sx={{ mt: 2 }}
                        gutterBottom
                    >
                        Pay failed!
                    </Typography>
                    <Typography
                        variant="body1"
                        sx={{ mt: 2 }}
                        gutterBottom
                    >
                        Sorry an error occurred.
                    </Typography>
                    <Typography
                        variant="body1"
                        sx={{ mt: 2 }}
                        gutterBottom
                    >
                        If an amount has been deducted from your account, it will be credited to your account within the last 24 hours.
                    </Typography>
                    <Button
                        variant="contained"
                        color="error"
                        onClick={() => history.push("/")}
                        sx={{
                            mt: 2,
                        }}
                        disableElevation
                        fullWidth
                    >
                        Back to home
                    </Button>
                </CardContent>
            </Card>
        </Box>
    );
}

export default FailedPage;