import {
    Box,
    Card,
    CardContent,
    Button,
    Typography,
} from "@mui/material";

import { useHistory, useParams } from "react-router-dom";

import SuccessPicture from "../../assets/success.svg";

const SuccessPage = () => {
    const history = useHistory();
    const params = useParams();

    return (
        <Box>
            <Card
                variant="elevation"
                elevation={18}
            >
                <CardContent sx={{ textAlign: "center" }}>
                    <Box
                        component="img"
                        alt="Failed picture"
                        src={SuccessPicture}
                    />
                    <Typography
                        variant="h4"
                        color="primary"
                        sx={{ mt: 2 }}
                        gutterBottom
                    >
                        Payed successfully!
                    </Typography>
                    <Typography
                        variant="body1"
                        sx={{ mt: 2 }}
                        gutterBottom
                    >
                        RefID: { params.refid }
                    </Typography>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => history.push("/")}
                        sx={{
                            mt: 2,
                        }}
                        disableElevation
                        fullWidth
                    >
                        Back to home
                    </Button>
                </CardContent>
            </Card>
        </Box>
    );
}

export default SuccessPage;