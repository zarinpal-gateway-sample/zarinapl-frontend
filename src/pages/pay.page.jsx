import { useEffect, useState } from "react";

import {
    Container,
    Card,
    CardHeader,
    CardContent,
    TextField,
    Button,
    Radio,
    RadioGroup,
    FormControl,
    FormLabel,
    FormControlLabel,
} from "@mui/material";

import Axios from "axios";

const gateways = [
    {
        value: "zarinpal",
        label: "Zarinpal",
        disable: false,
    },
    {
        value: "mellat",
        label: "Mellat",
        disable: false,
    },
    {
        value: "tejarat",
        label: "Tejarat",
        disable: true,
    },
    {
        value: "saderat",
        label: "Saderat",
        disable: true,
    },
    {
        value: "pasargod",
        label: "Pasargod",
        disable: true,
    },
];

const PayPage = () => {
    const env = process.env;

    const [amount, setAmount] = useState('1000');
    const [phone, setPhone] = useState('98');

    const [gateway, setGateway] = useState("zarinpal");

    const [refid, setRefid] = useState('');

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (refid) {
            document.getElementById("mellat").submit();
        }
    }, [refid]) 

    const continuePayment = () => {
        setLoading(true);

        const sendData = {
            Amount: amount,
            Phone: phone,
        }

        switch (gateway) {
            case "zarinpal": {
                Axios.post(`${env.REACT_APP_BACKEND_URL}/payment/zarinpal/request`, sendData)
                    .then((result) => {
                        setLoading(false);

                        window.location.replace(result.data.url);
                    })
                    .catch((error) => {
                        setLoading(false);

                        console.log(error);
                    });
                break;
            }
            case "mellat": {
                Axios.post(`${env.REACT_APP_BACKEND_URL}/payment/mellat/request`, sendData)
                    .then((result) => {
                        setLoading(false);

                        const incomingData = result.data;

                        if (incomingData.resCode === 0) {
                            setRefid(incomingData.refId);

                        } else {
                            console.log(incomingData)
                        }
                    })
                    .catch((error) => {
                        setLoading(false);

                        console.log(error);
                    });
                break;
            }
            default:
                console.log("Zarinpal");
                setLoading(false);
                break;
        }
    }

    return (
        <Container
            maxWidth="sm"
        >
            <form id="mellat" action="https://banktest.ir/gateway/pgw.bpm.bankmellat.ir/pgwchannel/startpay.mellat" method="POST">
                <input
                    type="text"
                    name="RefId"
                    value={refid}
                    onChange={(e) => setRefid(e.target.value)}
                    hidden
                />
            </form>
            <Card
                variant="elevation"
                elevation={20}
                sx={{
                    borderRadius: 1,
                }}
            >
                <CardHeader
                    title="Payment Gateways System"
                    sx={{
                        color: "primary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                />
                <CardContent>
                    <TextField
                        variant="outlined"
                        label="Amount"
                        placeholder="Enter amount ( In Toman )"
                        margin="normal"
                        value={amount}
                        onChange={(e) => setAmount(e.target.value)}
                        fullWidth
                    />
                    <TextField
                        variant="outlined"
                        label="Phone"
                        placeholder="Enter your phone number ( Eg: 98901xxxxxxx )"
                        margin="normal"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                        fullWidth
                    />
                    <FormControl margin="normal">
                        <FormLabel>Gateway</FormLabel>
                        <RadioGroup
                            value={gateway}
                            onChange={(e) => setGateway(e.target.value)}
                            row
                        >
                            {
                                gateways.map((gateway, index) => (
                                    <FormControlLabel
                                        key={gateway.label + ' ' + index}
                                        value={gateway.value}
                                        control={<Radio />}
                                        label={gateway.label}
                                        disabled={gateway.disable}
                                    />
                                ))
                            }
                        </RadioGroup>
                    </FormControl>
                    <Button
                        variant="contained"
                        onClick={() => !loading && continuePayment()}
                        sx={{
                            mt: 1
                        }}
                        disabled={loading && true}
                        disableElevation
                        fullWidth
                    >
                        { loading ? "Wait" : "Continue" }
                    </Button>
                </CardContent>
            </Card>
        </Container>
    );
}

export default PayPage;