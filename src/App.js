import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import {
  createTheme,
  ThemeProvider,
  CssBaseline,
  colors,
  Container,
} from "@mui/material";

import PayPage from "./pages/pay.page";

import SuccessPage from "./pages/status.pages/success.page";
import FailedPage from "./pages/status.pages/failed.page";

function App() {
  const theme = createTheme({
    palette: {
      background: {
        default: colors.teal[50],
      },
      primary: {
        main: colors.teal[500],
      }
    }
  });

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Router>
        <Container
          maxWidth="xl"
          sx={{
            height: "100vh",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Switch>
            <Route path="/" exact><PayPage /></Route>
            
            <Route path="/payment/success/:refid" exact><SuccessPage /></Route>
            <Route path="/payment/failed" exact><FailedPage /></Route>
            
          </Switch>
        </Container>
      </Router>
    </ThemeProvider>
  );
}

export default App;