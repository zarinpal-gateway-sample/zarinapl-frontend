# Zarinpal Front-End

This is a **MERN** project that we are going to learn about **Zarinpal Payment Gateway**.

## Techs

### Front-End

Front-End is a **Reactjs** application.

### Back-End

API is created with **ExpressJs**.

[Link to ExpressJs](https://gitlab.com/zarinpal-gateway-sample/zarinapl-api)

### Database

We use **MongoDB** as our database.

## How to run

### First clone and install deps

```bash
$ git clone https://gitlab.com/zarinpal-gateway-sample/zarinapl-frontend && cd zarinapl-frontend
$ npm i
```

### Then run application

```bash
$ npm start
```